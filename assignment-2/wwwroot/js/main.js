new Vue({
  el: "#testing",
  data: {
    Name: "",
    Contact: "",

    inputs: [
      {
        nameinput: "",
        contactinput: ""
      }
    ]
  },
  methods: {
    add(index) {
      this.inputs.push({ nameinput: this.Name, contactinput: this.Contact });
    },
    delete(index) {
      this.inputs.splice(index, 1);
    }
  }
});
