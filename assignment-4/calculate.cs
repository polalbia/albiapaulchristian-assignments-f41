using System;
 
namespace calculatejs{

	class Icalculator{

		 public void calculator(){ 
			Console.WriteLine("Basic Calculator");
            double firstnum, secondnum;  
            Console.Write("First Number : ");  
            firstnum = Convert.ToDouble(Console.ReadLine());  
            Console.Write("Second Number : ");  
        	secondnum = Convert.ToDouble(Console.ReadLine());  
            double Result;  
            Result = firstnum + secondnum;  
            Console.WriteLine("Answer : " + Result.ToString());  
            Console.ReadLine();  
        } 
	}

	class Isciecal{
		public void sciecal(){
			Console.WriteLine("Basic Scietific Calculation");
			double first;
			Console.Write("Enter First Number : ");
			first = Convert.ToDouble(Console.ReadLine());
			
			double ans = (first *(Math.PI)) / 180;
			Console.WriteLine(Math.Tan(ans));
			Console.ReadLine();
		}

	}

	class Iconversion{
		public void conversion() {
			Console.WriteLine("Basic Conversion");
			int biVal, decVal = 0, baseVal = 1, rem;
			Console.Write("Enter Binary Value : ");  
            biVal = Convert.ToInt32(Console.ReadLine());

			while (biVal > 0) {
            rem = biVal % 10;
            decVal = decVal + rem * baseVal;
            biVal = biVal / 10 ;
            baseVal = baseVal * 2;
            }
			Console.Write("Decimal: "+decVal);
         	Console.ReadLine();

        } 
	}

	class call{
		static void Main(string[]args){
			Icalculator calcu = new Icalculator();
			Isciecal scie = new Isciecal();
			Iconversion convert = new Iconversion();

			calcu.calculator();
			scie.sciecal();
			convert.conversion();
			System.Environment.Exit(1);
		}
	}
}