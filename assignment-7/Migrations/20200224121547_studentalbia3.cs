﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace assignment_7.Migrations
{
    public partial class studentalbia3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "MidName",
                table: "UserProfile",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(8) CHARACTER SET utf8mb4",
                oldMaxLength: 8);

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "UserProfile",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(8) CHARACTER SET utf8mb4",
                oldMaxLength: 8);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "UserProfile",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(8) CHARACTER SET utf8mb4",
                oldMaxLength: 8);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "MidName",
                table: "UserProfile",
                type: "varchar(8) CHARACTER SET utf8mb4",
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "UserProfile",
                type: "varchar(8) CHARACTER SET utf8mb4",
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "UserProfile",
                type: "varchar(8) CHARACTER SET utf8mb4",
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);
        }
    }
}
