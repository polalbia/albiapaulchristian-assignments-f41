using Microsoft.EntityFrameworkCore;

namespace assignment_7.Models
{
    public class StudentContext : DbContext
    {

        public StudentContext(DbContextOptions<StudentContext> options) : base(options)
        {
        }
        public DbSet<StudentReg> UserProfile { get; set; }
    }
}
