using System;
using System.ComponentModel.DataAnnotations;

namespace assignment_7.Models
{
    public class StudentReg
    {
         [Key]
        public int id { get; set; }


       [StringLength(50, ErrorMessage = "First Name length can't be more than 8.")]
        [Required]
        public string FirstName { get; set; }

        [StringLength(50, ErrorMessage = "Middle Name length can't be more than 8.")]
        [Required]
        public string MidName { get; set; }

        [StringLength(50, ErrorMessage = "Last Name length can't be more than 8.")]
        [Required]
        public string LastName { get; set; }

        [StringLength(50)]
        [Required]
        public string Gender { get; set; }

        [StringLength(50)]
        [Required]
        public string Address { get; set; }

       [Range(5,50, ErrorMessage = "Please Enter Valid Age")]
        [Required]
        public int Age { get; set; }
      

    }
}

